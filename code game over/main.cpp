#include <SFML/Graphics.hpp>
#define LARGEUR_FEN 1250
#define HAUTEUR_FEN 1000

using namespace sf;

int main()
{
    // Create the main window
    RenderWindow fenetre(VideoMode(LARGEUR_FEN, HAUTEUR_FEN), "Game Over");

    // Load a sprite to display
    Texture texture2;
    if (!texture2.loadFromFile("Game Over.png"))
        printf("Probleme de chargement de l'image !\n");
    Sprite over(texture2);
    over.setTexture(texture2);
    over.setPosition(0,0);

	// Start the game loop
    while (fenetre.isOpen())
    {
        // Process events
        Event event;
        while (fenetre.pollEvent(event))
        {
            // Close window : exit
            if (event.type == Event::Closed)
                fenetre.close();
        }

        // Clear screen
        fenetre.clear();

        // Draw the sprite
        fenetre.draw(over);

        // Update the window
        fenetre.display();
    }

    return EXIT_SUCCESS;
}
