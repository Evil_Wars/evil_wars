#include <SFML/Graphics.hpp>
using namespace sf;

#define BK "background.png"
#define BT "button.png"
#define FONT "dts.ttf"

int main()
{
    Font font;
    if(!font.loadFromFile("dts.ttf"))
    {
        printf("PB de chargement de l'image %s !\n", FONT);
    }

    RenderWindow fenetre(VideoMode(600, 900), "Menu");

    Texture fond;
    if (!fond.loadFromFile("background.png"))
        printf("PB de chargement de l'image %s !\n", BK);


    Sprite bk;
    bk.setTexture(fond);

    Text title;
    title.setFont(font);
    title.setString("EVIL WARS");
    title.setCharacterSize(100);
    title.setColor(Color::White);
    title.setStyle(Text::Underlined);
    title.setPosition(120,70);

    RectangleShape b1(Vector2f(220,60));
    b1.setFillColor(Color(201, 150, 54));
    b1.setOutlineThickness(5);
    b1.setOutlineColor(Color::White);
    b1.setPosition(195,320);

    RectangleShape b2(Vector2f(220,60));
    b2.setFillColor(Color(201, 150, 54));
    b2.setOutlineThickness(5);
    b2.setOutlineColor(Color::White);
    b2.setPosition(195,430);

    RectangleShape b3(Vector2f(220,60));
    b3.setFillColor(Color(201, 150, 54));
    b3.setOutlineThickness(5);
    b3.setOutlineColor(Color::White);
    b3.setPosition(195,540);

    RectangleShape b4(Vector2f(500,150));
    b4.setFillColor(Color(28, 28, 28));
    b4.setOutlineThickness(5);
    b4.setOutlineColor(Color::White);
    b4.setPosition(50,700);

    Text easy;
    easy.setFont(font);
    easy.setString("START");
    easy.setCharacterSize(40);
    easy.setStyle(Text::Underlined);
    easy.setColor(Color::White);
    easy.setPosition(265, 315);

    Text medium;
    medium.setFont(font);
    medium.setString("HIGH SCORE");
    medium.setCharacterSize(40);
    medium.setStyle(Text::Underlined);
    medium.setColor(Color::White);
    medium.setPosition(225, 425);

    Text hard;
    hard.setFont(font);
    hard.setString("SETTINGS");
    hard.setCharacterSize(40);
    hard.setStyle(Text::Underlined);
    hard.setColor(Color::White);
    hard.setPosition(235, 535);

    Text txt0;
    txt0.setFont(font);
    txt0.setString("Cr�ateurs :");
    txt0.setCharacterSize(20);
    txt0.setStyle(Text::Underlined);
    txt0.setColor(Color::White);
    txt0.setPosition(250, 710);

    Text txt1;
    txt1.setFont(font);
    txt1.setString("Henri Phothinantha, Leon Kamberi,");
    txt1.setCharacterSize(20);
    txt1.setColor(Color::White);
    txt1.setPosition(155, 740);

    Text txt2;
    txt2.setFont(font);
    txt2.setString("Yunus Haciagaoglu, Nguyen Quang Minh");
    txt2.setCharacterSize(20);
    txt2.setColor(Color::White);
    txt2.setPosition(140, 760);

    Text txt3;
    txt3.setFont(font);
    txt3.setString("Groupe 9");
    txt3.setCharacterSize(20);
    txt3.setStyle(Text::Underlined);
    txt3.setColor(Color::White);
    txt3.setPosition(260, 790);

    Text txt4;
    txt4.setFont(font);
    txt4.setString("Clic gauche : select");
    txt4.setCharacterSize(18);
    txt4.setColor(Color::White);
    txt4.setPosition(60, 820);

    Text txt5;
    txt5.setFont(font);
    txt5.setString("Clic droit : return");
    txt5.setCharacterSize(18);
    txt5.setColor(Color::White);
    txt5.setPosition(410, 820);

    // Load a sprite to display

    // Start the game loop
    while (fenetre.isOpen())
    {
        // Process events
        Event event;
        while (fenetre.pollEvent(event))
        {
            // Close window : exit
            if (event.type == Event::Closed)
                fenetre.close();
            if (event.type == sf::Event::MouseButtonPressed)
            {
                if(event.mouseButton.button == sf::Mouse::Left)
                {
                    if (easy.getGlobalBounds().contains(event.mouseButton.x, event.mouseButton.y))
                    {
                        b1.setFillColor(Color(114, 204, 102));
                        b2.setFillColor(Color(235, 203, 26));
                        b3.setFillColor(Color(209, 31, 31));
                        easy.setString("EASY");
                        easy.setPosition(270, 315);
                        medium.setString("MEDIUM");
                        medium.setPosition(250, 425);
                        hard.setString("HARD");
                        hard.setPosition(270, 535);
                    }

                }
                else if ((event.mouseButton.button == sf::Mouse::Right))
                {
                    b1.setFillColor(Color(201, 150, 54));
                    b2.setFillColor(Color(201, 150, 54));
                    b3.setFillColor(Color(201, 150, 54));
                    easy.setString("START");
                    easy.setPosition(265, 315);
                    medium.setString("HIGH SCORE");
                    medium.setPosition(225, 425);
                    hard.setString("SETTINGS");
                    hard.setPosition(235, 535);
                }
            }
        }


        fenetre.clear();

        fenetre.draw(bk);
        fenetre.draw(title);
        fenetre.draw(b1);
        fenetre.draw(b2);
        fenetre.draw(b3);
        fenetre.draw(b4);
        fenetre.draw(easy);
        fenetre.draw(medium);
        fenetre.draw(hard);
        fenetre.draw(txt0);
        fenetre.draw(txt1);
        fenetre.draw(txt2);
        fenetre.draw(txt3);
        fenetre.draw(txt4);
        fenetre.draw(txt5);
        fenetre.display();

    }

    system("pause");
    return EXIT_SUCCESS;

}
