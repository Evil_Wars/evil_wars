#include <SFML/Graphics.hpp>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>

#define LONGUEUR_FEN 1250
#define HAUTEUR_FEN 1000
#define TANK_LARGEUR 60
#define TANK_HAUTEUR 80


#define LARGEUR_BOWSER 60
#define HAUTEUR_BOWSER 80
#define LARGEUR_JASON 60
#define HAUTEUR_JASON 80
#define LARGEUR_GHOSTFACE 60
#define HAUTEUR_GHOSTFACE 80
#define LARGEUR_PREDATOR 60
#define HAUTEUR_PREDATOR 80

typedef  struct
{
    int x;
    int y;
} Tank;
typedef struct
{
    int px;
    int py;
} Missile;
typedef struct
{
    int x;
    int y;
} Route;
using namespace sf;
int main()
{
    Tank t;
    int tir = 0 ;
    int mechant = 0;
    Missile m;
    Route r;
    int pas=1;
    int PAS_route=1;
    float y=0;
    float PAS=1;
    int nb;
    srand(time(NULL));
    int vie=3;
    int score=0;

    char evenements[][30]= {"Closed", "Resized", "LostFocus", 
"GainedFocus",
                            "TextEntered", "KeyPressed", "KeyReleased", 
"MouseWheelMoved",
                            "MouseWheelScrolled", "MouseButtonPressed", 
"MouseButtonReleased",
                            "MouseMoved","MouseEntered", "MouseLeft", 
"JoystickButtonPressed",
                            "JoystickButtonReleased", "JoystickMoved", 
"JoystickConnected", "JoystickDisconnected",
                            "TouchBegan", "TouchMoved", "TouchEnded", 
"SensorChanged", "Count"
                           };
    char boutons [ ] [30]= {"Left", "Right", "Middle", "XButton1", 
"XButton2", "ButtonCount"};
    int const PAUSE = 10;
    RenderWindow fenetre(VideoMode(LONGUEUR_FEN, 
HAUTEUR_FEN,32),"Evil_Wars");
    Texture image3,image1,image2;

    Texture interface;
    if (!interface.loadFromFile("interface.png"))
        printf("Probleme de chargement de l'image!\n");
    Sprite it;
    it.setTexture(interface);
    it.setPosition(970,0);

    if (!image3.loadFromFile("route.jpg"))
    {
        printf("PB de chargement de l'image !\n");
    }
    Sprite route;
    route.setTexture(image3);
    //positionX=nb;
    r.x=-140;
    r.y=-500;
    route.setPosition(r.x,r.y);


    if (!image2.loadFromFile("missile.jpg"))
    {
        printf("PB de chargement de l'image !\n");
    }
    Sprite missile;
    missile.setTexture(image2);
    Event evenement;
    missile.setPosition(10,10);

    if (!image1.loadFromFile("perso.png"))
    {
        printf("PB de chargement de l'image !\n");
    }
    Sprite tank;
    tank.setTexture(image1);

    Texture bowser;
    if (!bowser.loadFromFile("bowser.png"))
        printf("Probleme de chargement de l'image!\n");
    Sprite bw;
    bw.setTexture(bowser);

    Texture jason;
    if (!jason.loadFromFile("jason.png"))
        printf("Probleme de chargement de l'image!\n");
    Sprite js;
    js.setTexture(jason);

    Texture ghostface;
    if (!ghostface.loadFromFile("ghostface.png"))
        printf("Probleme de chargement de l'image!\n");
    Sprite gf;
    gf.setTexture(ghostface);

    Texture predator;
    if (!predator.loadFromFile("predator.png"))
        printf("Probleme de chargement de l'image!\n");
    Sprite pd;
    pd.setTexture(predator);



    Sprite monstres[4] = {bw, js, gf, pd};


    nb = rand( ) % (900-60);
    int aleatoire = rand() % 4;

    while (fenetre.isOpen())
    {
        do{
        fenetre.setMouseCursorVisible(false);

        mechant = 1;
        while (fenetre.pollEvent(evenement))
        {
            switch (evenement.type)
            {
            case Event::Closed:
                fenetre.close();
                break;
            case Event::MouseMoved:
                if (evenement.mouseMove.y>=0 && evenement.mouseMove.x + 
80 <= 950 && evenement.mouseMove.x>=30 && evenement.mouseMove.y<=1180)
                {

                    
tank.setPosition(evenement.mouseMove.x-30,evenement.mouseMove.y-40) ;


                }
                break;
            case Event::KeyPressed:
                if(Keyboard::isKeyPressed(Keyboard::Escape))
                {
                    fenetre.close();
                }
                break;
            case Event::MouseButtonPressed:
                if (evenement.mouseButton.button == Mouse::Left )
                {
                    tir = 1;
                    m.px=tank.getPosition().x+10;
                    m.py=tank.getPosition().y-60;
                    missile.setPosition(m.px,m.py);
                }
                break;

            }

        }
        fenetre.clear(Color::Black);

        r.y+=PAS_route;
        if (r.y>=0)
        {
            r.y=-334;
        }
        route.setPosition(r.x,r.y);
        fenetre.draw(route);
        fenetre.draw(route);

        if (tir == 1 )
        {
            m.py-=pas;
            missile.setPosition(m.px,m.py);
            fenetre.draw(missile);
        }
        fenetre.draw(tank);
        fenetre.draw(it);



            fenetre.draw(monstres[aleatoire]);
            y+=PAS;
            monstres[aleatoire].setPosition(nb, y);

            FloatRect b_monstres = 
monstres[aleatoire].getGlobalBounds();
            FloatRect b_missile = missile.getGlobalBounds();
            FloatRect b_tank = tank.getGlobalBounds();

            if (  y > HAUTEUR_FEN)
            {
                y = 0;
                 nb = rand( ) % (900-60);
                aleatoire = rand() % 4;

                sleep(milliseconds(10));


            }

            if ((b_monstres.intersects(b_missile)) || 
(b_missile.intersects(b_monstres)))
            {
               tir = 0 ; y = HAUTEUR_FEN;

            }
            if ((b_tank.intersects(b_monstres)))
            {
                tir = 0 ; y = HAUTEUR_FEN;
                    sleep(milliseconds(10));

            }

        fenetre.display();
        sleep(milliseconds(5));
    }while (vie>0);




    }
    return 0;
}

