#include <SFML/Graphics.hpp>
#include <stdio.h>
#define LONGUEUR_FEN 1250
#define HAUTEUR_FEN 1500
#define TANK_LARGEUR 60
#define TANK_HAUTEUR 80
typedef  struct
{
    int x;
    int y;
} Tank;
typedef struct
{
    int px;
    int py;
} Missile;
typedef struct
{
    int x;
    int y;
} Route;
using namespace sf;
int main()
{
    Tank t;
    int tir = 0 ;
    Missile m;
    Route r;
    int pas=10;
    int PAS_route=1;
    char evenements[][30]= {"Closed", "Resized", "LostFocus", 
"GainedFocus",
                            "TextEntered", "KeyPressed", "KeyReleased", 
"MouseWheelMoved",
                            "MouseWheelScrolled", "MouseButtonPressed", 
"MouseButtonReleased",
                            "MouseMoved","MouseEntered", "MouseLeft", 
"JoystickButtonPressed",
                            "JoystickButtonReleased", "JoystickMoved", 
"JoystickConnected", "JoystickDisconnected",
                            "TouchBegan", "TouchMoved", "TouchEnded", 
"SensorChanged", "Count"
                           };
    char boutons [ ] [30]= {"Left", "Right", "Middle", "XButton1", 
"XButton2", "ButtonCount"};
    int const PAUSE = 10;
    RenderWindow fenetre(VideoMode(LONGUEUR_FEN, 
HAUTEUR_FEN,64),"Evil_Wars");
    Texture image3,image1,image2;
    if (!image3.loadFromFile("route.jpg"))
    {
        printf("PB de chargement de l'image !\n");
    }
    Sprite route;
    route.setTexture(image3);
    //positionX=nb;
    r.x=-140;
    r.y=0;
    route.setPosition(r.x,r.y);
    fenetre.draw(route);
    if (!image2.loadFromFile("missile.jpg"))
    {
        printf("PB de chargement de l'image !\n");
    }
    Sprite missile;
    missile.setTexture(image2);
    Event evenement;
    missile.setPosition(10,10);
    if (!image1.loadFromFile("perso.png"))
    {
        printf("PB de chargement de l'image !\n");
    }
    Sprite tank;
    tank.setTexture(image1);
//   tank.setOrigin(TANK_LARGEUR/2,TANK_HAUTEUR/2);
    printf(" origine du tank %lf %lf\n", tank.getOrigin().x, 
tank.getOrigin().y);

// tant que la fenêtre est ouverte
    while (fenetre.isOpen())
    {


// tant qu'il y a des évènements interceptés par la fenêtre
        while (fenetre.pollEvent(evenement))
        {
// je teste le type d’événement
            switch (evenement.type)
            {
// si fermeture de fenêtre déclenché
            case Event::Closed:
                fenetre.close();
                break;
//si touche appuyée ( et non saisie utilisateur !)
            case Event::MouseMoved:
                if (evenement.mouseMove.y>=0 && evenement.mouseMove.x + 
80 <= 950 && evenement.mouseMove.x>=30 && evenement.mouseMove.y<=1180)
                {

                    
tank.setPosition(evenement.mouseMove.x-30,evenement.mouseMove.y-40) ;


                }
                break;
            case Event::MouseButtonPressed:
                if (evenement.mouseButton.button == Mouse::Left )
                {
                    tir = 1;
                    
missile.setPosition(tank.getPosition().x+10,tank.getPosition().y-60);
                }
                break;

            }
            fenetre.clear(Color::Black);
            fenetre.draw(route);
            if (tir == 1 )
            {
                printf("je dessine le missile \n");
                //fenetre.clear(Color::Black);

                fenetre.draw(missile);
                //   missile.setPosition(missile.getPosition().x , 
missile.getPosition().y - pas );
            }
            fenetre.draw(tank);



            fenetre.display();

        }
    }
    return 0;
}

