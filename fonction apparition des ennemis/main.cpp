#include <SFML/Graphics.hpp>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>

using namespace sf;
#define LARGEUR_BOWSER 60
#define HAUTEUR_BOWSER 80
#define LARGEUR_JASON 60
#define HAUTEUR_JASON 80
#define LARGEUR_GHOSTFACE 60
#define HAUTEUR_GHOSTFACE 80
#define LARGEUR_PREDATOR 60
#define HAUTEUR_PREDATOR 80


int main()
{
    float y=0;
    float pas=0.1;
    int nb;
    srand(time(NULL));
    // Create the main window
    RenderWindow app(VideoMode(600, 900), "test ennemis");
        Texture bowser;
        if (!bowser.loadFromFile("bowser.png"))
            printf("Probleme de chargement de l'image!\n");
        Sprite bw;
        bw.setTexture(bowser);
        Texture jason;
        if (!jason.loadFromFile("jason.png"))
            printf("Probleme de chargement de l'image!\n");
        Sprite js;
        js.setTexture(jason);
        Texture ghostface;
        if (!ghostface.loadFromFile("ghostface.png"))
            printf("Probleme de chargement de l'image!\n");
        Sprite gf;
        gf.setTexture(ghostface);
        Texture predator;
        if (!predator.loadFromFile("predator.png"))
            printf("Probleme de chargement de l'image!\n");
        Sprite pd;
        pd.setTexture(predator);

        Sprite monstres[4] = {bw, js, gf, pd};

        //printf("%i", aleatoire);


    // Start the game loop
    while (app.isOpen())
    {
        while(1)
        {
            nb = rand( ) % (600-60);
            int aleatoire = rand() % 4;
            while (y<900-80)
            {


                monstres[aleatoire].setPosition(nb, y);
                y+=pas;
                // Clear screen
                app.clear();
                // Draw the sprite
                app.draw(monstres[aleatoire]);
                // Update the window
                app.display();

            }
            y=0;
            // Process events
            Event event;
            while (app.pollEvent(event))
            {
            // Close window : exit
            if (event.type == Event::Closed)
                app.close();
            }

        }

    }

}
