#include <SFML/Graphics.hpp>

using namespace sf;
#define LARGEUR_FEN 600
#define HAUTEUR_FEN 900
typedef struct{

int x;
int y;}Intro;

int main()
{
    Intro i;
    int PaS=1;
    // Create the main window
    RenderWindow fenetre(VideoMode(LARGEUR_FEN, HAUTEUR_FEN), "Intro");

    // Load a sprite to display
    Texture texture;
    if (!texture.loadFromFile("Texte Intro.png"))
        printf("Probleme de chargement de l'image !\n");
    Sprite Intro(texture);
    Intro.setTexture(texture);
    i.x=0;
    i.y=0;
    Intro.setPosition(i.x,i.y);

	// Start the game loop
    while (fenetre.isOpen())
    {
        // Process events
        Event event;
        while (fenetre.pollEvent(event))
        {
            // Close window : exit
            if (event.type == Event::Closed)
                fenetre.close();

        }

        // Clear screen
        fenetre.clear();

        // Draw the sprite
        i.y-=PaS;
        Intro.setPosition(i.x,i.y);
        fenetre.draw(Intro);
        sleep(milliseconds(10));
        if (i.y<-750)
        {
            system("pause");
            fenetre.close();

        }

        // Update the window
        fenetre.display();
    }

    return EXIT_SUCCESS;
}
