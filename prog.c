#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>


using namespace sf;

int main ( )
{


#define NOM_IMAGE "parachute.jpg"
#define LARGEUR_FEN 500
#define HAUTEUR_FEN 600
#define LARGEUR_BATEAU 160
#define HAUTEUR_BATEAU 120
#define LARGEUR_PARA 60
#define HAUTEUR_PARA 80

    int const PAUSE = 10;
    char evenements[][30]= {"Closed", "Resized", "LostFocus", 
"GainedFocus",
                            "TextEntered", "KeyPressed", "KeyReleased", 
"MouseWheelMoved",
                            "MouseWheelScrolled", "MouseButtonPressed", 
"MouseButtonReleased",
                            "MouseMoved","MouseEntered", "MouseLeft", 
"JoystickButtonPressed",
                            "JoystickButtonReleased", "JoystickMoved", 
"JoystickConnected", "JoystickDisconnected",
                            "TouchBegan", "TouchMoved", "TouchEnded", 
"SensorChanged", "Count"
                           };
    char boutons [ ] [30]= {"Left", "Right", "Middle", "XButton1", 
"XButton2", "ButtonCount"};
    int const PAS = 5 ;
    int const PAS2 = 20 ;
    char choix;

    int positionX;
    int positionY=0;
    int px;
    int py=0;
    srand(time(NULL));
    int nb;
    int nb2;




    RenderWindow 
fenetre(VideoMode(LARGEUR_FEN,HAUTEUR_FEN),"PARACHUTE");
    //fenetre.setFillColor(Color::White);
    Texture image1,image2;




    if (!image1.loadFromFile("parachute.jpg"))
    {
        printf("PB de chargement de l'image !\n");
    }
    Sprite para;
    para.setTexture(image1);
    positionX=nb;
    para.setPosition(nb,0);





    if (!image2.loadFromFile("bateau.jpg"))
    {
        printf("PB de chargement de l'image !\n");
    }
    Sprite bateau;
    bateau.setTexture(image2);
    nb = rand( ) % 440 ;
    px=nb;
    bateau.setPosition(nb,380);

    fenetre.draw(para);
    fenetre.draw(bateau);

    fenetre.display();

    Event evenement;

    while (fenetre.isOpen())
    {
        nb2 = rand( ) % 340;
        bateau.setPosition(nb2,480);
        fenetre.draw(bateau);
        fenetre.display();

        SoundBuffer buffer1,buffer2;

        if (!buffer1.loadFromFile("plouf.wav"))
            return -1;
        Sound sound(buffer1);
        if (!buffer2.loadFromFile("yeha.wav"))
            return -1;
        Sound sound2(buffer2);
        //sound.setBuffer(buffer);

do
{


        do
        {

            fenetre.clear(Color::White);
            for(int i=0; i < 5; i++)
            {
                int v=0;
                nb = rand( ) % 440 ;
                positionY = 0 ;
                para.setPosition(nb, positionY);



                while (true)
                {
                    fenetre.clear(Color::White);
                    positionY+=PAS;

                    if (positionY >= 600)
                    {



                        sound.play();
                        break;

                    }


                    // collision

                    FloatRect b_bateau = bateau.getGlobalBounds();
                    FloatRect b_para = para.getGlobalBounds();

                    if (b_bateau.intersects(b_para))
                    {
                        v++;
                        sound2.play();
                        printf ("%i vies sauvees\n",v);
                        break;

                    }


                    fenetre.draw(bateau);
                    bateau.setPosition(nb2,480);
                    para.setPosition(nb, positionY);
                    fenetre.draw(para); // la fenêtre dessine le 
rectangle
                    fenetre.display(); // la fenêtre affiche son ou ses 
dessins
                    sleep( milliseconds(PAUSE));

                    while (fenetre.pollEvent(evenement))
                    {

                        switch (evenement.type)
                        {

                        case Event::Closed:
                            fenetre.close();
                            system("exit");
                            break;

                        case Event::KeyPressed:
                            if (evenement.key.code == Keyboard::Left)
                            {

                                fenetre.clear(Color::White);
                                bateau.setPosition(nb2,480);
                                fenetre.draw(bateau);
                                fenetre.display();
                                nb2-=PAS2;
                                sleep(milliseconds(PAUSE));

                            }

                            if (evenement.key.code == Keyboard::Right)
                            {

                                fenetre.clear(Color::White);
                                bateau.setPosition(nb2,480);
                                fenetre.draw(bateau);
                                fenetre.display();
                                nb2+=PAS2;
                                sleep(milliseconds(PAUSE));
                            }
                            break;

                        }
                    }

                }
            }


            printf("Voulez vous recommencer?   O ou N\n");
            fflush(stdin);
            scanf("%c",&choix);

        }while (1);

}while(toupper(choix)=='O');

        return 0;
    }
}
